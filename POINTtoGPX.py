# -*- coding: utf-8 -*-
"""
/***************************************************************************
 POINTtoGPX
                                 A QGIS plugin
 Export points to gpx with labels name and desc
                              -------------------
        begin                : 2016-02-10
        git sha              : $Format:%H$
        copyright            : (C) 2016 by Piotr Szukała Lucatel Sp. z o.o.
        email                : piotr.szukala@lucatel.pl
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from PyQt4.QtCore import QSettings, QTranslator, qVersion, QCoreApplication
from PyQt4.QtGui import QAction, QIcon, QFileDialog
# Initialize Qt resources from file resources.py
import resources
# Import the code for the dialog
from POINTtoGPX_dialog import POINTtoGPXDialog
import os.path
from qgis.core import *
from qgis.gui import *
import qgis.utils


class POINTtoGPX:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'POINTtoGPX_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        # Create the dialog (after translation) and keep reference
        self.dlg = POINTtoGPXDialog()

        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&Point to GPX')
        # TODO: We are going to let the user set this up in a future iteration
        self.toolbar = self.iface.addToolBar(u'POINTtoGPX')
        self.toolbar.setObjectName(u'POINTtoGPX')

        #Dodaje funkjonalność otwarcia okna z wyborem miejsca zapisu pliku po kliknięciu wyboru lokalizacji
        self.dlg.lineEdit.clear()
        self.dlg.pushButton.clicked.connect(self.select_output_gpx)

    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('POINTtoGPX', message)


    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        icon_path = ':/plugins/POINTtoGPX/icon.png'
        self.add_action(
            icon_path,
            text=self.tr(u'POINTtoGPX'),
            callback=self.run,
            parent=self.iface.mainWindow())


    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(
                self.tr(u'&Point to GPX'),
                action)
            self.iface.removeToolBarIcon(action)
        # remove the toolbar
        del self.toolbar

         # Otwiera okno z wyborem pliku gdzie ma być zapisany plik gpx
    def select_output_gpx(self):
        self.dlg.lineEdit.clear()
        filename = QFileDialog.getSaveFileName(self.dlg, "Wybierz miejsce zapisu pliku ","", '*.gpx')
        self.dlg.lineEdit.setText(filename)



    def run(self):
        """Run method that performs all the real work"""
        layers = self.iface.legendInterface().layers()
        layer_list = []
        del layer_list[:]
        for layer in layers:
            layer_list.append(layer.name())
        self.dlg.comboBox_warstwa.clear()
        self.dlg.comboBox_warstwa.addItems(layer_list)


        selectedLayer = None
        selectedLayer = self.dlg.comboBox_warstwa

        #result_column = self.dlg.comboBox_column
        layer_filelds = None
        layer_filelds = self.dlg.comboBox_kolumna1

        layer_filelds2 = None
        layer_filelds2 = self.dlg.comboBox_kolumna2


        #self.run.columns = ''
        # Dodaje listę column do comboBox_column dla wybranej wcześniej warstwy
        def columns_of_selected_layer(self):
            selectedLayerIndex1 = selectedLayer.currentIndex()
            columns1 = layers[selectedLayerIndex1].pendingFields()
            column_list = []
            del column_list[:]
            for column in columns1:
                column_list.append(column.name())
            layer_filelds.clear()
            layer_filelds.addItems(column_list)
            #print selectedLayerIndex

        #selectedColumnIndex = self.dlg.comboBox_warstwa.currentIndex()

                # Dodaje listę column do comboBox_column dla wybranej wcześniej warstwy
        def columns_of_selected_layer2(self):
            selectedLayerIndex2 = selectedLayer.currentIndex()
            columns2 = layers[selectedLayerIndex2].pendingFields()
            column_list = []
            del column_list[:]
            for column in columns2:
                column_list.append(column.name())
            layer_filelds2.clear()
            layer_filelds2.addItems(column_list)
            #print selectedLayerIndex

        selectedColumnIndex = self.dlg.comboBox_warstwa.currentIndex()


        # Obsługa menu wyboru kolumny dla zaznaczonej warstwy. Używa SIGNAL activated dla podświetlonego obiektu i odpala funkcję columns_of_selected_layer
        LayerActivated = self.dlg.comboBox_warstwa.activated.connect(columns_of_selected_layer)
        LayerActivated = self.dlg.comboBox_warstwa.activated.connect(columns_of_selected_layer2)


        # show the dialog
        self.dlg.show()
        # Run the dialog event loop
        result = self.dlg.exec_()
        # See if OK was pressed
        if result:
            # Do something useful here - delete the line containing pass and
            # substitute with your code.

            # Definiowanie warstw
            selectedLayerIndexResult = self.dlg.comboBox_warstwa.currentIndex()
            selectedLayerResult = layers[selectedLayerIndexResult]

            vl = QgsVectorLayer("Point?crs=epsg:4326&field=name:string(250)&field=desc:string(250)", "temp", "memory")
            pr = vl.dataProvider()

            # transformacja ukladu wspolrzednych
            crsSrc = selectedLayerResult.crs()
            crsDest = QgsCoordinateReferenceSystem(4326)
            xform = QgsCoordinateTransform(crsSrc,crsDest)

            # selekcja wybranych kolumn
            selectedColumnIndex1 = self.dlg.comboBox_kolumna1.currentIndex()
            selectedColumnIndex2 = self.dlg.comboBox_kolumna2.currentIndex()

            # ustawienie zmiennej ścieżki zapisu pliku gpx
            file_gpx = self.dlg.lineEdit.text()

            # Dodawanie elementow do wirtualnej warstwy

            if selectedLayerResult.geometryType() == 0:
                tempFet = QgsFeature()

                for elem in selectedLayerResult.getFeatures():
                    geom = elem.geometry().asPoint()
                    geom = xform.transform(geom)
                    x = geom.x()
                    y = geom.y()
                    tempFet.setGeometry(QgsGeometry.fromPoint(geom))
                    tempFet.setAttributes([elem[selectedColumnIndex1],elem[selectedColumnIndex2]])
                    pr.addFeatures([tempFet])

                print "fields:", len(pr.fields())
                print "features:", pr.featureCount()

                # for f in pr.getFeatures():
                #     print f['name'], f['desc'], f.geometry().asPoint().x(), f.geometry().asPoint().y()

                #zapis do gpx- musza byc tylko kolumny o nazwie wspieranej przez gpx name i desc
                if "." not in file_gpx:
                    file_gpx = file_gpx + ".gpx"
                error = QgsVectorFileWriter.writeAsVectorFormat(vl,file_gpx, "UTF-8",None,"gpx")
                if error == QgsVectorFileWriter.NoError:
                    qgis.utils.iface.messageBar().pushMessage(u"Wszystko OK!", u"Plik zapisał się poprawnie", level=qgis.gui.QgsMessageBar.SUCCESS, duration=3)
            else:
                qgis.utils.iface.messageBar().pushMessage(u"Error", u"Wybrana warstwa nie jest warstwą punktową", level=qgis.gui.QgsMessageBar.WARNING, duration=3)

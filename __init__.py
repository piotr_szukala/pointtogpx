# -*- coding: utf-8 -*-
"""
/***************************************************************************
 POINTtoGPX
                                 A QGIS plugin
 Export points to gpx with labels name and desc
                             -------------------
        begin                : 2016-02-10
        copyright            : (C) 2016 by Piotr Szukała Lucatel Sp. z o.o.
        email                : piotr.szukala@lucatel.pl
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load POINTtoGPX class from file POINTtoGPX.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .POINTtoGPX import POINTtoGPX
    return POINTtoGPX(iface)
